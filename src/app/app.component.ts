import {Component, OnInit, Renderer, ElementRef} from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    title = 'Welcome to Image Search App';

    constructor(public element: ElementRef, public renderer: Renderer) {

    }
    ngOnInit() {
        document.body
        this.renderer.setElementStyle(document.body,'background-image','url(app/image-list/wallpaper-background.jpg)');
        this.renderer.setElementStyle(document.body,'background-repeat','no-repeat');
        this.renderer.setElementStyle(document.body,'background-size','1366px 698px');
    }
}