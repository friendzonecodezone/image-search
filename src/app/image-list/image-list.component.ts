import { Component, OnInit, ElementRef, Renderer} from '@angular/core';
import { ImageService } from '../shared/image.service';

@Component({
  selector: 'app-image-list',
  templateUrl: './image-list.component.html',
  styleUrls: ['./image-list.component.css']
})
export class ImageListComponent implements OnInit {
  images: any [];
  imagesFound: boolean = false;
  searching: boolean = false;



  constructor(private imageService: ImageService, public element: ElementRef, public renderer: Renderer) { }

  handleSuccess(data) {
    this.imagesFound = true;
    this.images = data.hits;
    console.log(data);
  }

  handleError(error) {
    console.log(error);
  }

  searchImages(query: string) {
    this.searching = true;
    return this.imageService.getImage(query)
    .subscribe(
      data => this.handleSuccess(data),
      error => this.handleError(error),
      () => this.searching = false
    )
  }

  ngOnInit() {
      this.renderer.listen(this.element.nativeElement.querySelector('input'),'click',()=>{
          this.renderer.setElementStyle(this.element.nativeElement.querySelector('input'),'background-color','white')
      })
  }
  

}
