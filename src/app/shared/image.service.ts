import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';

@Injectable ()

export class ImageService {
  private query: string;
  private API_KEY: string = environment.API_KEY;
  private API_URL: string = environment.API_URL;
  private URL: string = this.API_URL + this.API_KEY + '&q=';
  private perPage: string = "&per_page=12";

constructor(private http: Http) {}

getImage(query) {
  return this.http.get(this.URL + query + this.perPage)
  .map(Response => Response.json());
}
}
